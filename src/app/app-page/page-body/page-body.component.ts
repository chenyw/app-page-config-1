import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PageItem } from './../../viewmodel/page-item';
import { DataService } from '../data.service';

@Component({
  selector: 'app-page-body',
  templateUrl: './page-body.component.html',
  styleUrls: ['./page-body.component.scss']
})

export class PageBodyComponent implements OnInit {

  @Input()
  item! : PageItem;

  items: any[]; //設定下拉框某欄位為預設
  selected!: any[]; //設定ngModel，用來雙向繫結

  constructor(public pageSvc: DataService) {
    this.pageSvc.getAppPageInfos();

    this.items = [
      { name: "頁面代號", inactive: false},
      { name: "頁面標題", inactive: true},
      { name: "網頁路徑", inactive: true},
      { name: "輸出模組名稱", inactive: false},
      { name: "版本", inactive: false},
      { name: "維護人員", inactive: false}
    ];
  }
  ngOnInit() {
    /*this.pageSvc.getData().subscribe(res =>{
      this.items = res;
    })*/
  }
  editPage(page:PageItem){
    console.log(page);
  }
  deletePage(page:PageItem){
    console.log(page);
  }

  /*isUpdate($event:PageItem){
    //console.log($event);
    this.items = this.items.filter(item =>{
      return $event == item
    })
  }*/
}
