import { lastValueFrom } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PageItem } from '../viewmodel/page-item';
import { EmpItem } from '../viewmodel/emp-item';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  public appPageInfos: PageItem[] = []
  constructor(private http: HttpClient) { }
  public async getAppPageInfos(){
    const apiUrl = 'http://localhost:4200/assets/items.json';  //api url
    const apiContent = await lastValueFrom(this.http.get<PageItem[]>(apiUrl));  //api content
    this.appPageInfos = apiContent.map((value: { pageId: any; pageTitle: any; templateUrl: any; versionId: any; fontSize: any; remoteName: any; moduleName: any; maintainerNo: any; maintainerCode: any; maintainerName: any; systemUser: any; isUse: any; entityState: any; }) => ({
        "pageId": value.pageId,
        "pageTitle": value.pageTitle,
        "templateUrl": value.templateUrl?value.templateUrl:'null',
        "versionId": value.versionId,
        "fontSize": value.fontSize,
        "remoteName": value.remoteName? value.remoteName: 'null',
        "moduleName": value.moduleName? value.moduleName: 'null',
        "maintainerNo": value.maintainerNo,
        "maintainerCode":value.maintainerCode,
        "maintainerName":value.maintainerName,
        "systemUser":value.systemUser,
        "isUse":value.isUse,
        "entityState":value.entityState
    }));
    return this.appPageInfos;
  }

  public empInfos: EmpItem[] = []
  public async getDevelopers(){
    const apiUrl = 'http://localhost:4200/assets/emps.json';  //api url
    const apiContent = await lastValueFrom(this.http.get<EmpItem[]>(apiUrl));  //api content
    this.empInfos = apiContent.map(value => ({
        "empNo":value.empNo,
        "empCode":value.empCode,
        "empName":value.empName
    }));
    return this.empInfos;
  }

  /*getData(){
    return this.http.get<PageItem[]>('http://localhost:4200/assets/items.json');
  }*/

}
